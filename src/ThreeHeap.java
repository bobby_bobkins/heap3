// Jordan Courvoisier
// Petr Shuller
// CS 240 with Ryan Parsons
// Assignment 2


import java.util.Arrays;
import java.util.List;

// A three children heap implementation. Provides standard heap methods, as well as an efficient buildQueue method
public class ThreeHeap implements PriorityQueue {

    // Default size of internal array
    private static final int defaultSize = 128;

    // Amount of elements currently in heap
    private int size;

    // Internal representation of the heap
    private double[] items;

    // Default Constructor
    public ThreeHeap () {
        this(defaultSize);
    }
   
    // Constructor that uses specified size
    // Pre: Desired initial size of internal array
    public ThreeHeap (int size) {
        this.size = 0;
        items = new double[size];
    }

    // Constructor that builds heap from list
    // Pre: element list of type double
    public ThreeHeap (List<Double> list) {
        buildQueue(list);
    }

    // Post: Returns true if heap is empty
    public boolean isEmpty() {
        return size == 0;
    }

    // Post: Returns number of elements in heap
    public int size() {
        return size;
    }

    // Pre: Throws EmptyHeapException when called on an empty heap
    // Post: Returns lowest priority value item
    public double findMin() {
        if (isEmpty()) {
            throw new EmptyHeapException();
        }

        return items[1];
    }

    // Pre: Number to be inserted, duplicates are allowed
    // Post: Places number in correct place in heap
    public void insert(double x) {
        if (++size == items.length) {
            increaseSize();
        }
        items[size] = x;

        // Only percolate up if not inserting first item
        if (size !=  1) {
            percolateUp(size, (size + 1) / 3);
        }
    }

    // Pre: Throws EmptyHeapException when called on an empty heap
    // Post: Returns minimum element
    public double deleteMin() {
        if (isEmpty()) {
            throw new EmptyHeapException();
        }

        double min = findMin();

        swap(1, size);
        size--;
        percolateDown(1);

        return min;
    }

    // Pre: List of doubles
    // Post: Creates a new array using given values, and turns it into a valid heap
    //       Duplicates are allowed
    //       Previous values are no longer accessible
    public void buildQueue(List<Double> list){

        size = list.size();
        items = new double[size + 1];

        for (int i = 1; i <= size; i++) {
            items[i] = list.get(i-1);
        }
    
        for (int i = (size + 1) / 3; i > 0; i--) {
            percolateDown(i);
        }
    }

    // Post: Makes heap to appear empty, previous values are no longer accessible
    public void makeEmpty() {
        size = 0;
    }

    // Post: Returns an array string representation of heap
    public String toString() {
        double[] array = new double[size];
        for (int i = 1; i <= size; i++){
            array[i - 1] = items[i];
        }

        return Arrays.toString(array);
    }

    // Pre: Index is parent of num
    // Post: Percolates item up to correct position
    private void percolateUp (int index, int indexParent) {
        if (items[index] >= items[indexParent] || index == 1){
            return;
        }

        swap(indexParent, index);

        percolateUp ( indexParent, (indexParent + 1) / 3);
    }

    // Post: Increases size of underlying array
    private void increaseSize() {
        items = Arrays.copyOf(items, size * 3);
    }

    // Pre: Index of item to percolate down
    // Post: Percolates an item down to proper position
    private void percolateDown (int index) {
        int minChild = minChild(index);

        // Make sure we have valid index, and we are greater than our children
        if (minChild > 0 && items[minChild] < items[index]) {
            swap(index, minChild);
            percolateDown(minChild);
        }
    }

    // Pre: Parent's index whose smallest child we are trying to find
    // Post: Returns index of smallest child of passed in index
    private int minChild(int index) {
        int childOne = index * 3 + 1;
        int childTwo = index * 3;
        int childThree = index * 3 - 1;

        int indexMin = childThree;

        if (childThree <= size) {
        	if (childTwo <= size && items[childTwo] < items[indexMin]) {
        	    indexMin = childTwo;
        	}
        	if (childOne <= size && items[childThree] < items[indexMin]) {
        	    indexMin = childThree;
        	}
        } else {
            return -1; // We don't have children
        }

        return indexMin;
    }

    // Pre: Two indices that need to be swapped
    // Post: Swaps given elements
    private void swap(int indexOne, int indexTwo) {
        double temp = items[indexOne];
        items[indexOne] = items[indexTwo];
        items[indexTwo] = temp;
    }
}