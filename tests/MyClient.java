// Jordan Courvoisier
// Petr Shuller
// CS 240 with Ryan Parsons
// Assignment 2

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Class to run some unit and integration tests.
public class MyClient {
    @Test
    public void testTest() {
        Assert.assertTrue(true);
    }

    @Test
    public void testInit () {
        // Test Default Constructor
        ThreeHeap threeHeapDefault = new ThreeHeap();
        Assert.assertTrue(threeHeapDefault.isEmpty());
        Assert.assertEquals(0, threeHeapDefault.size());

        // Test Constructor with size parameter
        ThreeHeap threeHeapSize = new ThreeHeap(20);
        Assert.assertTrue(threeHeapSize.isEmpty());
        Assert.assertEquals(0, threeHeapSize.size());

        // Test Constructor with list parameter
        List<Double> list = new ArrayList<Double>();

        for (int i  = 0; i < 100; i++) {
            list.add((double)i);
        }

        ThreeHeap threeHeapList = new ThreeHeap(list);
        Assert.assertFalse(threeHeapList.isEmpty());
        Assert.assertEquals(list.size(), threeHeapList.size());
    }

    @Test
    public void integrationTest () {
        // Create and populate list for initial constructor
        List<Double> list = new ArrayList<Double>();
        for (int i  = 0; i < 10; i++) {
            list.add((double)i);
        }

        ThreeHeap threeHeap = new ThreeHeap(list);

        // Should be at some initial size
        Assert.assertFalse(threeHeap.isEmpty());
        Assert.assertEquals(list.size(), threeHeap.size());

        // Make sure build heap is working correctly
        Assert.assertEquals(Arrays.toString(list.toArray()), threeHeap.toString());

        // Min should be 0
        Assert.assertEquals(0 , threeHeap.findMin(), 0);

        Assert.assertEquals(0, threeHeap.deleteMin(), 0);

        Assert.assertEquals("[1.0, 4.0, 2.0, 3.0, 9.0, 5.0, 6.0, 7.0, 8.0]", threeHeap.toString());

        // Testing emptying heap
        threeHeap.makeEmpty();
        Assert.assertTrue(threeHeap.isEmpty());
        Assert.assertEquals(0, threeHeap.size(), 0);
        Assert.assertEquals("[]", threeHeap.toString());

        // Test insert
        threeHeap.insert(9); threeHeap.insert(1); threeHeap.insert(3);
        Assert.assertFalse(threeHeap.isEmpty());
        Assert.assertEquals(3, threeHeap.size(), 0);
        Assert.assertEquals("[1.0, 9.0, 3.0]", threeHeap.toString());

        // Test find, delete
        Assert.assertEquals(1.0, threeHeap.findMin(), 0);
        Assert.assertEquals(1.0, threeHeap.deleteMin(),0);
        Assert.assertEquals(2, threeHeap.size(),0);
        Assert.assertFalse(threeHeap.isEmpty());
        Assert.assertEquals("[3.0, 9.0]", threeHeap.toString());
        threeHeap.makeEmpty();

        // Test buildQueue, use list we already made
        threeHeap.buildQueue(list);
        Assert.assertEquals("[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0]", threeHeap.toString());
        threeHeap.makeEmpty();


        // Test duplicates
        threeHeap.insert(1); threeHeap.insert(1);
        Assert.assertEquals("[1.0, 1.0]", threeHeap.toString());

        // Should be empty after this
        threeHeap.deleteMin(); threeHeap.deleteMin();
        Assert.assertEquals(0, threeHeap.size());
        Assert.assertTrue(threeHeap.isEmpty());

        // Test for some exceptions
        try {
            threeHeap.findMin();
            System.err.println("Should of thrown exception when finding min on empty heap");
        } catch (EmptyHeapException e){
            // Test passed
        }
        try {
            threeHeap.deleteMin();
            System.err.println("Should of thrown exception when deleting min on empty heap");
        } catch (EmptyHeapException e){
            // Test passed
        }

        // Final tests to ensure empty heap is at expected state
        Assert.assertTrue(threeHeap.isEmpty());
        Assert.assertEquals(0, threeHeap.size(),0);
    }
}