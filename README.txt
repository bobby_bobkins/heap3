// Jordan Courvoisier
// Petr Shuller
// CS 240 with Ryan Parsons
// Assignment 2

Write-up Questions:

1.Please briefly discuss how you went about testing your heap implementation.
   
    Used the JUnit framework and wrote some unit tests and a comprehensive integration test. Made sure that heap was being initialized as expected,
    and then performed all operations that the PriorityQueue interface requires. Checked edge cases and made sure exceptions were being thrown as necessary.
    We were able to catch a number of small math errors, so that was nice.

2. Big-O Analysis of your Heap: What is the worst case big-O running time of buildHeap, isEmpty, size, insert, findMin, and deletemin operations on your heap.
   [For this analysis, you should ignore the cost of growing the array. That is, assume that you have enough space when you are inserting a value.]
   
    isEmpty: O(1), a simple boolean check.
    
    size: O(1), accessing single member.
    
    findMin: O(1), as we are accessing first element.
    
    deleteMin:
        O(logn): everything is constant, except for the percolate down that happens. To percolate down our program finds the smallest child, swaps the smallest with the node being percolated, 
        and repeats until it is a leaf or is smaller than all of it's children. Since we are cutting the problem size in three at each step, this is logn(base 3), or just O(logn).
  
    insert: O(logn): Since at worst case our heap will have to percolate the entire height of the tree, we have a logn(base 3) = O(log(n)) runtime for insert.
        
    buildHeap: θ(n): We effectively start with a bunch of little heaps, and are percolating down level by level going basically bottom - 1 to top.
                     This is Floyd's method to build heaps, which we know is θ(n)


3.Asymptotic runtime: For each of the following program fragments, determine the asymptotic runtime in terms of n.

  O(nlogn)
        a. public void mysteryOne(int n) {

  1       int sum = 0;

  n       for (int i = n; i >= 0; i--) {

  1          if ((i % 5) == 0) {
  1               break;
  1          } else {
  logn            for (int j = 1; j < n; j*=2) {
  1                 sum++;
              }
            }
          }
        }


   O(n^3)
        b. public void mysteryTwo(int n) {

  1        int x = 0;

  n        for (int i = 0; i < n; i++) {

  n^2          for (int j = 0; j < (n * (n + 1) / 3); j++) {

  1            x += j;

            }

          }

        }


   O(n^2)
        c. public void mysteryThree(int n) {

   n       for (int i = 0; i < n; i++) {

   n         printCats(n);

          }

        }



   O(n)
        public void printCats(int n) {

   n        for (int i = 0; i < n; i++) {

   1         System.out.println("catsmoop");

          }

        }



4. Psuedocode and Runtime:

    a. Write pseudocode for a function that calculates the largest difference between any two numbers in an array of positive integers with a runtime in Θ(n2).
    For example, the largest difference between any two numbers in the array storing the values [4, 6, 3, 9, 2, 1, 20] would be 19.

        min, max

        for every number
            check against all others.
            If smaller, min = number, if larger than all others, max = number

        return max - min
       

    b. Can this function be written with a runtime Θ(n)? If yes, write the pseudocode. If no, why? What would have to be different about the input to do so?

        min = first value
        max = first value

        for num in array:
            if num greater than max, max = num
            if num less than min, min = num

        return max - min

    c. Can this function be written with a runtime Θ(1)? If yes, write the pseudocode. If no, why? What would have to be different about the input to do so?

        Yes, if the array is sorted:
        return abs(array[0] - array[size - 1])